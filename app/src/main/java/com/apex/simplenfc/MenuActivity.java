package com.apex.simplenfc;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

/**
 * Created by JOAM on 24-03-2020.
 */

public class MenuActivity  extends AppCompatActivity {
    Helpers helpers;
    int counterEgg;
    SharedPreferences sharedPref;
    TextView txt_key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            helpers.AlertDialog("Internet Permission not granted",this);
        }else{
            Log.i("Permisos:","Permiso para acceder a internet concedido");
        }

        //declara el boton para volver al menu
        final Button btn_marks = (Button) findViewById(R.id.btn_marks);
        //lanza la nueva pantalla al clickear el boton
        btn_marks.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });


        //declara el boton para volver al menu
        final Button btn_incidents = (Button) findViewById(R.id.btn_incidents);
        //lanza la nueva pantalla al clickear el boton
        btn_incidents.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), IncidentsActivity.class);
                startActivity(intent);
            }
        });


        /*
        //declara el boton para entrar a la configuracion
        final Button btn_config = (Button) findViewById(R.id.btn_config);
        //lanza la nueva pantalla al clickear el boton
        btn_config.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ConfigActivity.class);
                startActivity(intent);
            }
        });
         */

        sharedPref = getSharedPreferences("myPref", MODE_PRIVATE);
        txt_key = (TextView) findViewById(R.id.txt_key);
        //carga la llave si es que existe
        String instance_key = sharedPref.getString("instance_key", "");
        if(instance_key ==""){
            Log.i("KeyInfo", " La key no existe");
        }else{
            txt_key.setText("K: " + instance_key);
        }
    }


    public void easterEgg(View view){
        counterEgg = counterEgg + 1;

        if(counterEgg==10){
            counterEgg = 0;
            Intent intent = new Intent(getApplicationContext(), ConfigActivity.class);
            startActivity(intent);
        }


    }

}
