package com.apex.simplenfc.Models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by JOAM on 26-03-2020.
 */

public class Pictures implements Serializable {

    String uri;
    String name;
    int incident_id;
    String description;
    Date created_at;
    String thumb_uri;
    int id;

    public Pictures(String uri, String name, int incident_id, String description, Date created_at, int id,String thumb_uri) {
        this.uri = uri;
        this.name = name;
        this.incident_id = incident_id;
        this.description = description;
        this.created_at = created_at;
        this.id = id;
        this.thumb_uri = thumb_uri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIncident_id() {
        return incident_id;
    }

    public void setIncident_id(int incident_id) {
        this.incident_id = incident_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumb_uri() {
        return thumb_uri;
    }

    public void setThumb_uri(String uri) {
        this.thumb_uri = thumb_uri;
    }

}
