package com.apex.simplenfc.Models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by JOAM on 25-03-2020.
 */

public class Incidents implements Serializable {
    public int id;
    public Date created_at;
    public String title;
    public String description;
    public int incidenttype_id;
    public int approved;

    public Incidents(String title, String description, int incidenttype_id, String guard) {
        this.title = title;
        this.description = description;
        this.incidenttype_id = incidenttype_id;
        this.guard = guard;
    }

    public String guard;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIncidenttype_id() {
        return incidenttype_id;
    }

    public void setIncidenttype_id(int incidenttype_id) {
        this.incidenttype_id = incidenttype_id;
    }

    public int isApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public int getApproved() {
        return approved;
    }

    public String getGuard() {
        return guard;
    }

    public void setGuard(String guard) {
        this.guard = guard;
    }

}
