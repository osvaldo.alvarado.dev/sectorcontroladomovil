package com.apex.simplenfc;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import okhttp3.Response;

/**
 * Created by JOAM on 27-03-2020.
 */

public class TakePictureActivity extends AppCompatActivity {
    int incident_id;
    Button btn_take_picture,btn_send_picture;
    EditText txt_description;
    public byte[]  byteStringImage;
    private String mImageFileLocation = "";
    public ImageView img_picture;
    Helpers helpers;
    public File image;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_picture);

        //obtiene el id del intent anterior
        incident_id = getIntent().getExtras().getInt("id");

        txt_description = (EditText) findViewById(R.id.txt_description);
        btn_take_picture = (Button) findViewById(R.id.btn_take_picture);
        btn_send_picture = (Button) findViewById(R.id.button_send_picture);
        img_picture = (ImageView) findViewById(R.id.image_picture);

        btn_send_picture.setEnabled(false);
        /*Enviar datos de la fotografia*/
        btn_send_picture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    sendPicture();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

          /*Enviar datos de la fotografia*/
        btn_take_picture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent callCameraApplicationIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));

                startActivityForResult(callCameraApplicationIntent,1);
            }
        });


//Chequeo que existe el permiso para LEER el external Storage
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        Log.e("Pemiso: ",String.valueOf(permissionCheck));


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    File createImageFile() throws IOException {
        //  String imageFileName = "foto";

        //Creamos una carpeta en la memeria del terminal
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "DCIM");
        imagesFolder.mkdirs();
        //añadimos el nombre de la imagen
        image = new File(imagesFolder, "foto.jpg");
        Uri uriSavedImage = Uri.fromFile(image);

        //   File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

        //   File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();

        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        try{
            //Comprovamos que la foto se a realizado

            Log.e("RequestCode:",String.valueOf(requestCode));
            if (requestCode == 1 && resultCode == RESULT_OK) {
                rotateImage(setReducedImageSize());

                btn_send_picture = (Button) this.findViewById(R.id.button_send_picture);
                btn_send_picture.setEnabled(true);
            }
        }catch(Exception e){
            Log.e("ErrorEnResultado","1");

        }
    }

    private void rotateImage(Bitmap bitmap){
        ExifInterface exifInterface = null;
        try{
            exifInterface = new ExifInterface(mImageFileLocation);
        } catch(IOException e){
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation){
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            default:
        }
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);

        byteStringImage = getByteArrayImage(rotatedBitmap);
        img_picture.setImageBitmap(rotatedBitmap);
    }

    private Bitmap setReducedImageSize() {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mImageFileLocation, bmOptions);

        bmOptions.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(mImageFileLocation, bmOptions);
    }

    //retorna bytearray desde un archivo bitmap
    private byte[] getByteArrayImage(Bitmap bitmapPicture) {

        try{
            final int COMPRESSION_QUALITY = 50;
            String encodedImage;
            ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
            bitmapPicture.compress(Bitmap.CompressFormat.JPEG, COMPRESSION_QUALITY, byteArrayBitmapStream);
            byte[] b = byteArrayBitmapStream.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return b;
        }catch(Exception e){
            Log.e("ErrorGetByteArray",e.toString());
            return null;
        }

    }

    public void sendPicture() throws IOException, JSONException {

            try{
                btn_send_picture = (Button) this.findViewById(R.id.button_send_picture);
                btn_send_picture.setEnabled(true);

                String description = txt_description.getText().toString();

                final File imageFile = image;

                Response response = helpers.postPicture("http://10.0.2.2:8080/sectorcontroladoweb/public/api/pictures",incident_id,description, imageFile );

                if(response.code() == 201){
                    helpers.AlertDialog("Se guardo Correctamente la imagen",TakePictureActivity.this);
                    btn_send_picture.setEnabled(false);

                    img_picture.setImageResource(R.mipmap.picture_no_avalible);
                    txt_description.setText("");
                }else{
                    helpers.AlertDialog("No se guardo correctamente la imagen",TakePictureActivity.this);
                }

            }catch(Exception e){
                Log.e("ErrorSendPicture",e.toString());
            }



    }
}
