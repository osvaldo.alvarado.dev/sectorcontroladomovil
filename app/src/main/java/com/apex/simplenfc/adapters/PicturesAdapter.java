package com.apex.simplenfc.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import com.apex.simplenfc.Models.Pictures;
import com.apex.simplenfc.R;

import java.net.URL;
import java.util.ArrayList;
import com.apex.simplenfc.Helpers;
/**
 * Created by JOAM on 26-03-2020.
 */

public class PicturesAdapter extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<Pictures> items;
    Helpers helpers = new Helpers();


    public PicturesAdapter(Activity activity, ArrayList<Pictures> items) {
        this.activity = activity;
        this.items = items;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<Pictures> category) {
        for (int i = 0; i < category.size(); i++) {
            items.add(category.get(i));
        }
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.activity_adapter_list,null);
        }

        Pictures dir = items.get(position);

        TextView title = (TextView) v.findViewById(R.id.txt_title);





        int incidentTypeId =  dir.getId();
        try{


            ImageView imagen = (ImageView) v.findViewById(R.id.img_logo);
            String publicUriString = dir.getThumb_uri();
            //Bitmap bitmap = helpers.TransformUriToPicture(publicUriString);
            URL url = new URL(publicUriString);
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            imagen.setImageBitmap(bmp);
            Log.e("PublicUri " , publicUriString);
            if(bmp != null){
                imagen.setImageBitmap(bmp);
            }


        }catch(Exception e){


        }


        title.setText("Fotografia Id: " + dir.getId());

        //el subtitulo se deja para rellenar con otro dato
        TextView description = (TextView) v.findViewById(R.id.txt_subtitle);
        String subtitulo = dir.getDescription();
        description.setText(subtitulo);

        return v;
    }



}
