package com.apex.simplenfc.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apex.simplenfc.Models.Incidents;
import com.apex.simplenfc.R;

import java.util.ArrayList;

/**
 * Created by JOAM on 25-03-2020.
 */

public class IncidentsAdapter extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<Incidents> items;


    public IncidentsAdapter(Activity activity, ArrayList<Incidents> items) {
        this.activity = activity;
        this.items = items;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<Incidents> category) {
        for (int i = 0; i < category.size(); i++) {
            items.add(category.get(i));
        }
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.activity_adapter_list,null);
        }

        Incidents dir = items.get(position);

        TextView title = (TextView) v.findViewById(R.id.txt_title);


        ImageView imagen = (ImageView) v.findViewById(R.id.img_logo);

        imagen.setImageResource(R.mipmap.ic_launcher);
        int incidentTypeId =  dir.getIncidenttype_id();
        String prioridad = "";
        switch (incidentTypeId)
        {
            case 1 :
                prioridad = "Alta";
                imagen.setImageResource(R.mipmap.warning_red);
                break;

            case 2 :
                prioridad = "Media";
                imagen.setImageResource(R.mipmap.warning_yellow);
                break;

            case 3:
                prioridad = "Baja";
                imagen.setImageResource(R.mipmap.warning_green);
                break;
        }


        title.setText(dir.getId() + " - " + dir.getTitle() + " [" + prioridad + "]");

        //el subtitulo se deja para rellenar con otro dato
        TextView description = (TextView) v.findViewById(R.id.txt_subtitle);
        String subtitulo = dir.getDescription();
        description.setText(subtitulo);

        return v;
    }
}
