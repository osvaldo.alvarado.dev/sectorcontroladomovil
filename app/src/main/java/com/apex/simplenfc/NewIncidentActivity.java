package com.apex.simplenfc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.apex.simplenfc.Models.Incidents;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSerializationContext;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Response;

/**
 * Created by JOAM on 24-03-2020.
 */

public class NewIncidentActivity extends AppCompatActivity {

    // initialize SharedPreferences var
    SharedPreferences sharedPref;
    TextView txt_key;
    Helpers helpers;
    Button btn_save_incident;
    EditText txt_title,txt_guard,txt_description;
    Spinner spinner_priority;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_incident);

        //declara el boton para volver al menu
        final Button btn_menu_principal = (Button) findViewById(R.id.btn_back);

        //instancia los textos y el spinner
        txt_title = (EditText) findViewById(R.id.txt_title);
        txt_description = (EditText) findViewById(R.id.txt_description);
        txt_guard = (EditText) findViewById(R.id.txt_guard);
        spinner_priority = (Spinner) findViewById(R.id.spinner_priority);
        btn_save_incident = (Button) findViewById(R.id.btn_save_incident);

        //lanza la nueva pantalla al clickear el boton
        btn_menu_principal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), IncidentsActivity.class);
                startActivity(intent);
            }
        });

        //genera array para el spinner
        String[] arraySpinner = new String[] {
               "Alta","Media","Baja"
        };

        //pasa los parametros para el spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_priority.setAdapter(adapter);
        //setea en el control "Prioridad BAJA"
        spinner_priority.setSelection(2);


        //strict mode para poder hacer tareas como post y get
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        //lanza la nueva pantalla al clickear el boton
        btn_save_incident.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //instancia una nueva incidencia
                Incidents incident = new Incidents(txt_title.getText().toString(),txt_description.getText().toString(),spinner_priority.getSelectedItemPosition() + 1,txt_guard.getText().toString());

                //convierte la incidencia en un nuevo elemento json
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                String incidentJson =  gson.toJson(incident);

                //carga la llave si es que existe para pasarla como header
                sharedPref = getSharedPreferences("myPref", MODE_PRIVATE);
                String instance_key = sharedPref.getString("instance_key", "");
                try {
                    Response response =  helpers.postIncident("http://10.0.2.2:8080/sectorcontroladoweb/public/api/incidents",incidentJson,instance_key);

                    Log.i("Response:",response.toString());
                    if(response.code() == 201){
                        //instancia el objeto obtenido
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        helpers.AlertDialog("Se creo correctamente la incidencia",NewIncidentActivity.this);
                        resetControls();
                        Intent intent = new Intent(getApplicationContext(), IncidentDetailsActivity.class);
                        //pasa parametros a la pantalla siguiente
                        intent.putExtra("id", Integer.parseInt(jsonObject.get("id").toString()));
                        startActivity(intent);
                    }else{
                        helpers.AlertDialog("No se pudo crear la incidencia, error:" + response.body().string(),NewIncidentActivity.this);

                    }
                    //helpers.AlertDialog("Se creo correctamente la Incidencia",getApplicationContext());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void resetControls(){
        txt_title.setText("");
        txt_description.setText("");
        txt_guard.setText("");
        //setea en el control "Prioridad BAJA"
        spinner_priority.setSelection(2);

    }


    /*
    *
    *   txt_title = (EditText) findViewById(R.id.txt_title);
        txt_description = (EditText) findViewById(R.id.txt_description);
        txt_guard = (EditText) findViewById(R.id.txt_guard);
        spinner_priority =
    * */



}
