package com.apex.simplenfc;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.apex.simplenfc.Models.Incidents;
import com.apex.simplenfc.Models.Pictures;
import com.apex.simplenfc.adapters.IncidentsAdapter;
import com.apex.simplenfc.adapters.PicturesAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Response;

/**
 * Created by JOAM on 26-03-2020.
 */

public class IncidentDetailsActivity extends AppCompatActivity {
    //declaro variables para usar en esta clase
    int incident_id;
    Helpers helpers;
    Response response;
    Incidents incident;
    ArrayList<Pictures> picturesArrayList = new ArrayList<Pictures>();
    TextView txt_id,txt_title,txt_description,txt_guard,txt_priority,txt_approbed;
    Button btn_add_picture;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //strict mode para poder hacer tareas asincronicas (metodos get)
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_details);
        incident_id = getIntent().getExtras().getInt("id");


        //instancia los campos visuales
        txt_id = (TextView) findViewById(R.id.txt_id);
        txt_title = (TextView) findViewById(R.id.txt_title);
        txt_description = (TextView) findViewById(R.id.txt_description);
        txt_guard = (TextView) findViewById(R.id.txt_guard);
        txt_priority = (TextView) findViewById(R.id.txt_priority);
        btn_add_picture = (Button) findViewById(R.id.btn_add_picture);
        txt_approbed = (TextView) findViewById(R.id.txt_approbed);



        btn_add_picture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TakePictureActivity.class);
                intent.putExtra("id", incident.getId());
                startActivity(intent);
            }
        });


        loadIncident();

    }


    @Override
    public void onResume(){
        super.onResume();
        loadIncident();
    }


    public void loadIncident(){
        try {
            response = Helpers.genericGetRequest("http://10.0.2.2:8080/sectorcontroladoweb/public/api/incidents/" + String.valueOf(incident_id));

            if(response.code() == 200){
                String incidentJson = response.body().string();
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
                Gson gson = gsonBuilder.create();
                //convierte el json en listado del elemento seleccionado
                incident = gson.fromJson(incidentJson, new TypeToken<Incidents>(){}.getType());

                txt_id.setText(String.valueOf(incident.getId()));
                txt_guard.setText(incident.getGuard());
                txt_title.setText(incident.getTitle());
                txt_description.setText("Descripcion : " + incident.getDescription());

                //si el incidente esta finalizado no deja ponerle mas fotografias
                if(incident.getApproved() == 1){

                    btn_add_picture.setEnabled(false);

                    txt_approbed.setText("Informe Finalizado");
                    txt_approbed.setTextColor(Color.GREEN);
                }else{
                    btn_add_picture.setEnabled(true);
                    txt_approbed.setText("Informe Pendiente de Revision");
                    txt_approbed.setTextColor(Color.RED);
                }


                //setear prioridad
                String prioridad = "";
                switch (incident.getIncidenttype_id()){
                    case 1:
                        prioridad = "Alta";
                        break;
                    case 2 :
                        prioridad = "Media";
                        break;
                    case 3:
                        prioridad = "Baja";
                        break;
                }
                txt_priority.setText(prioridad);

                //Oculta el teclado al cargar la pantalla
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                //genera el listado de fotografias
                JSONObject jsonObject = new JSONObject(incidentJson);
                String picturesJson = jsonObject.get("pictures").toString();
                picturesArrayList = gson.fromJson(picturesJson, new TypeToken<ArrayList<Pictures>>(){}.getType());

                //instancia el list view para poder pasar los elementos
                ListView picturesListView = (ListView) findViewById(R.id.list_pictures);
                //instancia adaptador con la lista de elementos
                final PicturesAdapter adapter = new PicturesAdapter(this,picturesArrayList);
                //setea el adaptador
                picturesListView.setAdapter(adapter);

            }else{

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
