package com.apex.simplenfc;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by JOAM on 18-08-2019.
 */

public class Helpers {


    public static String postRequest(String url, String jsonBody) throws IOException {
        OkHttpClient client;
        MediaType JSON;
        client = new OkHttpClient();
        JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("card_id", jsonBody)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        Log.e("Respuesta :",response.toString());
        return response.body().string();
    }


    public static String postGeneric(String url, String jsonBody) throws IOException {
        OkHttpClient client;
        MediaType JSON;
        client = new OkHttpClient();
        JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, jsonBody);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        Log.e("Respuesta :",response.toString());
        return response.body().string();
    }

    public static Response postIncident(String url, String jsonBody,String key) throws IOException {
        OkHttpClient client;
        MediaType JSON;
        client = new OkHttpClient();
        JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, jsonBody);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("key",key)
                .build();
        Response response = client.newCall(request).execute();
        Log.e("Respuesta :",response.toString());
        return response;
    }



    public static String getRequest(String url,String key) throws IOException {


        try{
            OkHttpClient client;
            MediaType JSON;
            client = new OkHttpClient.Builder()
                    .build();
            JSON = MediaType.parse("application/json; charset=utf-8");
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("key",key)
                    .build();
            Response response = client.newCall(request).execute();
            Log.e("Respuesta :",response.toString());
            return response.body().string();
        }catch(Exception e){
            Log.e("OkHTTP",e.toString());
            Log.e("OkHTTP","URL" + url);
            return null;
        }

    }

    public static Response genericGetRequest(String url) throws IOException {

        try{
            OkHttpClient client;
            MediaType JSON;
            client = new OkHttpClient.Builder()
                    .build();
            JSON = MediaType.parse("application/json; charset=utf-8");
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            Log.e("Respuesta :",response.toString());
            return response;
        }catch(Exception e){
            Log.e("OkHTTP",e.toString());
            Log.e("OkHTTP","URL" + url);
            return null;
        }

    }

    public static void AlertDialog(String title, Context context) {
        //mContext = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public Bitmap TransformUriToPicture(String URI){

        try{
            Bitmap bitmap = DownloadImage(URI);

            if(bitmap != null){
                return bitmap;
            }
            else{
                Bitmap voidBitmap = null;
                return voidBitmap;
            }
        }catch(Exception e){
            Log.e("Exepction","Error al cargar la imagen");
            Bitmap voidBitmap = null;
            return voidBitmap;
        }


        /*  Helper para transformar URIS a bitmap, luego operar con el bitmap
        *   ImageView img = (ImageView) findViewById(R.id.img);
        *   img.setImageBitmap(bitmap);
        * */
    }

    private Bitmap DownloadImage(String URL) {
        Bitmap bitmap = null;
        InputStream in = null;
        try {
            in = OpenHttpConnection(URL);
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
            return bitmap;
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            Log.e("IOexeption","Error IO");
            Bitmap voidBitmap = null;
            Bitmap icon = BitmapFactory.decodeResource(null,R.mipmap.ic_launcher);
            return voidBitmap;
        }

    }

    private InputStream OpenHttpConnection(String urlString) throws IOException {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");

        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            throw new IOException("Error connecting");
        }
        return in;
    }



    public static Response postPicture(String url,int incident_id,String description,File imageFile) throws IOException {

        try{
            final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

            OkHttpClient client;
            client = new OkHttpClient();

            RequestBody bodyRequest = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("picture", imageFile.getName(),
                            RequestBody.create(MediaType.parse("image/jpg"), imageFile))
                    .addFormDataPart("incident_id",String.valueOf(incident_id))
                    .addFormDataPart("description",description)
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .post(bodyRequest)
                    .build();
            Response response = client.newCall(request).execute();

            Log.e("Respuesta :",response.toString());
            return response;


        }catch(Exception e){
            Log.e("ErrorPicture",e.toString() + e.getMessage());
            return null;
        }


    }
}
