package com.apex.simplenfc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.apex.simplenfc.Models.Incidents;
import com.apex.simplenfc.adapters.IncidentsAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by JOAM on 24-03-2020.
 */

public class IncidentsActivity  extends AppCompatActivity {
    Helpers helpers;
    ArrayList<Incidents> incidentsList = new ArrayList<Incidents>();
    // initialize SharedPreferences var
    SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //strict mode para poder hacer tareas asincronicas (metodos get)
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incidents);


        //declara el boton para volver al menu
        final Button btn_menu_principal = (Button) findViewById(R.id.btn_back);
        //lanza la nueva pantalla al clickear el boton
        btn_menu_principal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });


        //declara el boton para volver al menu
        final Button btn_new_incident = (Button) findViewById(R.id.btn_new_incidence);
        //lanza la nueva pantalla al clickear el boton
        btn_new_incident.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), NewIncidentActivity.class);
                startActivity(intent);

            }
        });

        loadIncidents();

    }

    @Override
    public void onResume(){
        super.onResume();
        loadIncidents();
    }


    public void loadIncidents(){
        try {
            //carga la llave si es que existe para pasarla como header
            sharedPref = getSharedPreferences("myPref", MODE_PRIVATE);
            String instance_key = sharedPref.getString("instance_key", "");

            //obtiene las incidencias desde el server
            //String incidents = helpers.getRequest("http://app.sectorcontrolado.cl/api/incidents");
            String incidents = helpers.getRequest("http://10.0.2.2:8080/sectorcontroladoweb/public/api/incidents",instance_key);
            if(incidents != null){
                Log.i("Datos:",incidents);
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
                Gson gson = gsonBuilder.create();
                String jsonTxt = incidents;
                //convierte el json en listado del elemento seleccionado
                incidentsList = gson.fromJson(jsonTxt, new TypeToken<ArrayList<Incidents>>(){}.getType());

                //instancia el list view para poder pasar los elementos
                ListView incidentsListView = (ListView) findViewById(R.id.lst_items);
                //instancia adaptador con la lista de elementos
                final IncidentsAdapter adapter = new IncidentsAdapter(this,incidentsList);
                //setea el adaptador
                incidentsListView.setAdapter(adapter);


                incidentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        if(true){
                            //obtengo la posicion presionada, instancio un objeto Picture y le paso los parametros
                            //seleccionados
                            final int pos = position;
                            final int elementId = incidentsList.get(pos).getId();
                            Log.e("ID Capturado",String.valueOf(elementId));
                            Intent intent = new Intent(getApplicationContext(), IncidentDetailsActivity.class);
                            //pasa parametros a la pantalla siguiente
                            intent.putExtra("id", elementId);
                            startActivity(intent);

                        }
                    }
                });


            }else{
                helpers.AlertDialog("No se ha podido cargar ninguna incidencia de hoy ;(",this);
            }
        } catch (IOException e) {
            Log.e("Error:",e.toString());
        } catch (Exception e){

            Log.e("Error:",e.toString());
        }
    }

}
