package com.apex.simplenfc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by JOAM on 24-03-2020.
 */

public class ConfigActivity  extends AppCompatActivity {

    // initialize SharedPreferences var
    SharedPreferences sharedPref;
    Button btn_save_key;
    TextView txt_key;
    Helpers helpers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config);



        //declara el boton para volver al menu
        final Button btn_menu_principal = (Button) findViewById(R.id.btn_back);
        //lanza la nueva pantalla al clickear el boton
        btn_menu_principal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });

       btn_save_key = (Button) findViewById(R.id.btn_save_key);


        //guarda la llave
        btn_save_key.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               saveKey();
            }
        });


        // get or create SharedPreferences
        sharedPref = getSharedPreferences("myPref", MODE_PRIVATE);
        txt_key = (TextView) findViewById(R.id.txt_key);
        //carga la llave si es que existe
        String instance_key = sharedPref.getString("instance_key", "");
        if(instance_key ==""){
            Log.i("KeyInfo", " La key no existe");
        }else{
            txt_key.setText(instance_key);
        }
    }


    public void saveKey(){
        // get or create SharedPreferences
        sharedPref = getSharedPreferences("myPref", MODE_PRIVATE);
        // save your string in SharedPreferences
        sharedPref.edit().putString("instance_key", txt_key.getText().toString()).commit();

        helpers.AlertDialog("La llave ha sido guardada correctamente en el dispositivo ;)",this);
    }


}
