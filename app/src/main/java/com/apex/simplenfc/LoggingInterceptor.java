package com.apex.simplenfc;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by JOAM on 25-03-2020.
 */

public class LoggingInterceptor  implements Interceptor {
    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        Log.e("Interceptor :",
                request.url().toString() + chain.connection().toString() + request.headers().toString());

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Log.e("Interceptor 2:",
                response.request().url().toString() +  String.valueOf((t2 - t1) / 1e6d) + response.headers().toString());

        return response;
    }

}
