package com.apex.simplenfc;

/**
 * Created by JOAM on 18-08-2019.
 */

public class Register {

    public Integer id;
    public String card_title;
    public String error;
    public String round_name;


    public Register(Integer id, String card_title, String error,String round_name) {
        this.id = id;
        this.card_title = card_title;
        this.error = error;
        this.round_name = round_name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCard_title() {
        return card_title;
    }

    public void setCard_title(String card_title) {
        this.card_title = card_title;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {

        return error;
    }



    public String getRound_name() {
        return round_name;
    }

    public void setRound_name(String round_name) {
        this.round_name = round_name;
    }
}
