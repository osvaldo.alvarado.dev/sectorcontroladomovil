package com.apex.simplenfc;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.apex.simplenfc.NFC.NFCInterface;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements NFCInterface{

    TextView txtNFCID,txtSerialNumber,txtHistory,txtCardTitle;
    private Subscription nfcSubscription;
    Helpers helpers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //desabilita la orientacion de la app
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        txtNFCID=(TextView)findViewById(R.id.txtNFCID);
        txtSerialNumber = (TextView)findViewById(R.id.txtSerialNumber);
        txtHistory = (TextView)findViewById(R.id.txtHistory);
        txtCardTitle = (TextView)findViewById(R.id.txtCardTitle);

        //estas 2 lineas son necesarias para bajar el nivel del main para poder ejecutar peticiones http
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //declara el boton para volver al menu
        final Button btn_menu_principal = (Button) findViewById(R.id.btn_menu_principal);
        //lanza la nueva pantalla al clickear el boton
        btn_menu_principal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });

        //instancia el boton para resetear la lectura
        final Button button2 = (Button) findViewById(R.id.button1);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               //txtNFCID.setText("sin lectura");
                txtSerialNumber.setText("--");
            }
        });

        //instancia el boton para enviar datos
        final Button btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //envia datos al server
                AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
                asyncTaskRunner.serialNumber = "62FEA281";
                asyncTaskRunner.execute();
            }
        });


        //cambio el titulo
        getSupportActionBar().setTitle("Sector Controlado V20190818");
    }

    @Override
    protected void onResume() {
        super.onResume();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter filter = new IntentFilter();
        filter.addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_TECH_DISCOVERED);


        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        try{
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{filter}, this.techList);
        }catch(Exception e){
            Log.e("Error NFC",e.toString());
            helpers.AlertDialog("Encienda el Receptor NFC del Dispositivo",this);
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        // disabling foreground dispatch:
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        try{
            nfcAdapter.disableForegroundDispatch(this);
        }catch (Exception e){
            Log.e("Error NFC","No se pudo desactivar el background del NFC ya que nunca estubo habilitado");
        }



    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {

            String type = intent.getType();
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            byte[] id = tag.getId();
            final String serialNumber = bytesToHex(id);

            Log.i("AhoraSe",serialNumber);

            //envia datos al server
            AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
            asyncTaskRunner.serialNumber = serialNumber;
            asyncTaskRunner.execute();

            nfcReader(tag);
        }
    }


    @Override
    public String nfcRead(Tag t) {
        try {
            Tag tag = t;
            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                return null;
            }


            NdefMessage ndefMessage = ndef.getCachedNdefMessage();
            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records)
            {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT))
                {
                    try {return readText(ndefRecord);} catch (UnsupportedEncodingException e) {}
                }
            }
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }

    @Override
    public String readText(NdefRecord record) throws UnsupportedEncodingException {
        byte[] payload = record.getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

        byte[] idByte = record.getId();


        String s = new String(idByte);
        String str = new String(idByte, "UTF-8"); // for UTF-8 encoding
        Log.i("ID",str);


        String hexdump = new String();
        for (int i = 0; i < payload.length; i++) {
            String x = Integer.toHexString(((int) payload[i] & 0xff));
            if (x.length() == 1) {
                x = '0' + x;
            }
            hexdump += x + ' ';
        }
        Log.i("NUMERODESERIE",hexdump.toString());


        int languageCodeLength = payload[0] & 0063;
        return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
    }

    @Override
    public void nfcReader(Tag tag) {

        nfcSubscription= Observable.just(nfcRead(tag))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(String s) {
                        if (s != null) {
                            Log.e("Cadena",s);
                            //txtNFCID.setText(s);

                        }
                    }
                });



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unsubscribe(nfcSubscription);

    }


    private static void unsubscribe(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
            subscription = null;
        }
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }


    public void sendData(View view) throws IOException, JSONException {


        Log.i("ButtonPress","Button presionado");

        txtHistory.setText("Boton presionado");
    }





    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        private String resp;
        ProgressDialog progressDialog;
        public String serialNumber;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                //String response = helpers.postRequest("http://192.168.1.6:8080/SectorControladoWeb/public/api/marks",serialNumber);
                String response = helpers.postRequest("http://sectorcontrolado.codigominga.cl/api/marks",serialNumber);
                Log.i("Response:",response);
                resp = response;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            txtHistory.setText(result);

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Gson gson = gsonBuilder.create();

            txtSerialNumber.setText(serialNumber);
            Date currentTime = Calendar.getInstance().getTime();
            String dateString;
            Calendar calendario = Calendar.getInstance();
            Date fecha = calendario.getTime();
            System.out.println(fecha);
            SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            dateString = formatoDeFecha.format(currentTime).toString();

            try {
                JSONObject respuestaJsonGet = new JSONObject(result);
                Register register = gson.fromJson(result, Register.class);


                if(register.getId() != null){
                    txtHistory.setText(String.format("Se registro correctamente N: " + register.getId() + "\nFecha: " + dateString));
                    txtHistory.setBackgroundColor(Color.GREEN);
                    txtCardTitle.setText("Sector : " + register.card_title);

                    if(register.getRound_name() != null){
                        txtNFCID.setText("Ronda : "  + register.getRound_name());
                        txtNFCID.setBackgroundColor(Color.GREEN);
                    }else{
                        txtNFCID.setText("Ronda : No se registro ninguna ronda dentro del horario");
                        txtNFCID.setBackgroundColor(Color.YELLOW);
                    }


                }else{
                    txtHistory.setText("Error " + register.getError());
                    txtHistory.setBackgroundColor(Color.RED);
                    txtCardTitle.setText("--");
                    txtNFCID.setText("");

                }



            } catch (Exception e) {
                txtHistory.setText("Error al registrar"+ " fecha: " + currentTime.toString());
                txtHistory.setBackgroundColor(Color.RED);
                e.printStackTrace();

            }

        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "Enviando Punto de Control",
                    "Triangulando");
        }


        @Override
        protected void onProgressUpdate(String... text) {
            txtHistory.setText("cargando");
            txtHistory.setBackgroundColor(Color.YELLOW);

        }
    }
    /** Called when the user taps the Send button */



}
